import keras
import tensorflow as tf
import cv2
import itertools
import numpy as np
import os
from matplotlib import pyplot as plt
from keras.preprocessing import image
from keras import backend as K 
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, StratifiedKFold
from keras.layers import Dense, Conv2D, MaxPool2D, Dropout, Flatten
from keras.callbacks import ModelCheckpoint, EarlyStopping
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score

clases = ['rest', 'squat', 'pullup', 'pushup']
K.clear_session()

def evaluate(model, x_valid, y_valid):
    return model.evaluate(x_valid, y_valid)

def plot_confusion_matrix(cm, classes, title, acc,
                          normalize=False,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center")

    plt.tight_layout()
    plt.gcf().subplots_adjust(bottom=0.4)
    plt.ylabel('True label')
    plt.xlabel('Predicted label\nAcuraccy: ' + acc)
    plt.savefig(title + '.png')
    plt.clf()
    
## multiclass or binary report
## If binary (sigmoid output), set binary parameter to True
def full_multiclass_report(model,
                           model_name,
                           x,
                           y_true,
                           classes,
                           title,
                           batch_size=5,
                           binary=False):

    # 1. Transform one-hot encoded y_true into their class number
    if not binary:
        y_true = np.argmax(y_true,axis=1)
    
    # 2. Predict classes and stores in y_pred
    y_pred = model.predict_classes(x)
    
    # 3. Print accuracy score
    print("Accuracy : "+ str(accuracy_score(y_true,y_pred)))
    
    print("")
    
    # 4. Print classification report
    print("Classification Report: " + model_name)
    print(classification_report(y_true,y_pred,digits=5))    
    
    # 5. Plot confusion matrix
    cnf_matrix = confusion_matrix(y_true,y_pred)
    print(cnf_matrix)
    plot_confusion_matrix(cnf_matrix,classes=classes, title=title, acc=str(accuracy_score(y_true,y_pred)))


def load_data_2_channels(movement, obj):
    directories = open('./datasets/augmented/flow/' + movement + '/' + obj + '/files.txt', 'r')
    x_train = []
    y_train = []
    for video in directories:
        train_flows = []
        DIR = './datasets/augmented/flow/' + movement + '/' + obj + '/' + video.rstrip()
        total_files = 0
        total_files = len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))])
        if (total_files < 24):
            continue
        for flow in range(1, int(total_files / 2) + 1):
            flow_x = cv2.imread('./datasets/augmented/flow/' + movement + '/' + obj + '/' + video.rstrip() + '/flow_x' + str(flow) + '.jpg', cv2.IMREAD_GRAYSCALE)
            flow_y = cv2.imread('./datasets/augmented/flow/' + movement + '/' + obj + '/' + video.rstrip() + '/flow_y' + str(flow) + '.jpg', cv2.IMREAD_GRAYSCALE)

            gray_flow_x = cv2.resize(flow_x, (100, 100))
            # gray_flow_x = gray_flow_x[...,::-1]

            gray_flow_y = cv2.resize(flow_y, (100, 100))
            # gray_flow_y = gray_flow_y[...,::-1]

            train_flows.append(cv2.merge((gray_flow_x, gray_flow_y)))
            if (train_flows.__len__() % 12 == 0):
                x_train.append(np.array(np.concatenate(train_flows, axis = -1))) 
                y_train.append(clases.index(movement))
                train_flows = []

    x_train = np.array(x_train)
    # y_train = [[clases.index(movement)]] * len(data)
    # y_train = [clases.index(movement)] * len(data)
    return (x_train, y_train)


squat_x_valid, squat_y_valid = load_data_2_channels('squat', 'valid')

pushup_x_valid, pushup_y_valid = load_data_2_channels('pushup', 'valid')

pullup_x_valid, pullup_y_valid = load_data_2_channels('pullup', 'valid')

rest_x_valid, rest_y_valid = load_data_2_channels('rest', 'valid')

# Normalization

x_valid = np.concatenate((squat_x_valid, pushup_x_valid, pullup_x_valid, rest_x_valid))
y_valid = np.concatenate((squat_y_valid, pushup_y_valid, pullup_y_valid, rest_y_valid))

# x_valid = x_valid / 255
y_valid = keras.utils.to_categorical(y_valid, len(clases))

# Load models

directory = './with_rest_v1'

# for filename in os.listdir(directory):
filename = os.listdir(directory)[0]
print(filename + '\n')
for weights in os.listdir('./with_rest_v1/' + filename + '/training') :
    model = None
    del model

    if weights.endswith(".h5"):


        # model = get_model(len(clases))

        model = keras.models.load_model('./with_rest_v1/' + filename + '/training/' + weights)

        # evaluate(model, x_valid, y_valid)

        weights_name, weights_file_extension = os.path.splitext(weights)

        # FINISH THIS
        full_multiclass_report(model, weights_name, x_valid, y_valid, clases, title = './with_rest_v1/' + filename + '/testing/' + weights_name + '_Confusion_Matrix')