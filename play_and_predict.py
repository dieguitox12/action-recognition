import keras
import tensorflow as tf
import cv2
import itertools
import numpy as np
import os
import sys
from matplotlib import pyplot as plt
from keras.preprocessing import image
from keras import backend as K 
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, StratifiedKFold
from keras.layers import Dense, Conv2D, MaxPool2D, Dropout, Flatten
from keras.callbacks import ModelCheckpoint, EarlyStopping
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
import subprocess
import calendar
import time
from keras.utils.vis_utils import plot_model

clases = ['rest', 'squat', 'pullup', 'pushup']
K.clear_session()

def load_data_2_channels(ts):
    data = []
    x = []
    train_flows = []
    files = os.listdir('./tmp/' + ts)
    for flow in range(1, int(files.__len__() / 2) + 1):

        flow_x = cv2.imread('./tmp/' + ts + '/flow_x' + str(flow) + '.jpg', cv2.IMREAD_GRAYSCALE)
        flow_y = cv2.imread('./tmp/' + ts + '/flow_y' + str(flow) + '.jpg', cv2.IMREAD_GRAYSCALE)

        gray_flow_x = cv2.resize(flow_x, (100, 100))
        # gray_flow_x = gray_flow_x[...,::-1]

        gray_flow_y = cv2.resize(flow_y, (100, 100))
        # gray_flow_y = gray_flow_y[...,::-1]

        train_flows.append(cv2.merge((gray_flow_x, gray_flow_y)))

        if (train_flows.__len__() % 12 == 0):
            x.append(np.array(np.concatenate(train_flows, axis = -1))) 
            train_flows = []

    return np.array(x)


def load_data(raw_video_path):
    ts = str(calendar.timegm(time.gmtime()))
    os.mkdir('tmp/' + ts)
    avi_video = raw_video_path

    cap = cv2.VideoCapture(avi_video)
    total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    proc = subprocess.Popen(
        [
            './denseFlow_gpu', 
            '--vidFile=' + avi_video, 
            '--xFlowFile=' + 'tmp/' + ts + '/flow_x', 
            '--yFlowFile=' + 'tmp/' + ts + '/flow_y', 
            '--device_id=0',
            '--type=2',
            '--bound=16',
            '--clip=20000000',
            '--step=3'
        ], 
        stdout=subprocess.PIPE, 
        stderr=subprocess.PIPE
    )

    prev = -1
    while True:
        out = proc.stdout.read(1)
        if out == b'':
            while True:
                err = proc.stderr.readline()
                if err == b'':
                    break
                else:
                    print(err)
            break
        if out != b'':
            files = os.listdir('./tmp/' + ts).__len__() / 2
            if prev != files: 
                prev = files
                sys.stdout.write("Analyzing: " +  str(round((files * 100) / (total_frames / 3))) + "%\n")
                sys.stdout.flush()

    # stdout,stderr = out.communicate()

    print('Processing...\n')
    # if (stderr == None):
    return load_data_2_channels(ts)
    # return False

# cap = cv2.VideoCapture(sys.argv[1])
# total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
# # cap.set(cv2.CAP_PROP_POS_AVI_RATIO, 1)
# fps = cap.get(cv2.CAP_PROP_FPS)      # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
# # frameCount = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
# duration = total_frames / fps

flows = load_data(sys.argv[1])
print(flows.shape)
if (isinstance(flows, bool)):
    sys.exit('Something failed')

# model = keras.models.load_model('v2/no_dropout/training/best_mixed_original_fold_2_val_acc.h5')

# y_pred = model.predict_classes(flows)
# y_pred2 = model.predict(flows)

# if (cap.isOpened()== False): 
#     print("Error opening video stream or file")

current_frame = 0

# print(y_pred)
# print(y_pred2)

movement = '???'

filename = os.listdir('with_rest_v2')[0]
total_pred = []
total_pred2 = []
print('Using: ' + filename)

for weights in os.listdir('with_rest_v2/' + filename + '/training') :
    model = None

    if weights.endswith(".h5"):

        current_frame = 0

        movement = '???'

        model = keras.models.load_model('with_rest_v2/' + filename + '/training/' + weights)
        plot_model(model, to_file='model_plot.png', show_shapes=True, show_layer_names=True)
        exit
        # y_pred = model.predict_classes(flows)
        y_pred2 = model.predict(flows)

        total_pred2.append(y_pred2)

average = total_pred2[0]
for preds in range(1, total_pred2.__len__()):
    average += total_pred2[preds]

average = average / total_pred2.__len__()

print(average)
total_pred = average.argmax(axis = 1)
print(total_pred)

cap = cv2.VideoCapture(sys.argv[1])
total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
# cap.set(cv2.CAP_PROP_POS_AVI_RATIO, 1)
fps = cap.get(cv2.CAP_PROP_FPS)      # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
# frameCount = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
duration = total_frames / fps
        
# Read until video is completed
while(cap.isOpened()):
    ret, frame = cap.read()

    font                   = cv2.FONT_HERSHEY_SIMPLEX
    bottomLeftCornerOfText = (0, 30)
    fontScale              = 0.8
    fontColor              = (0, 0, 255)
    lineType               = 2


    # total_frames / y_pred.__len__()
    index = current_frame / 36
    i, d = divmod(index, 1)
    i = int(i)

    same_prev = True
    same_post = True

    if (i < total_pred.__len__()): 
        if (average[i][total_pred[i]] > 0.4):
            movement = clases[total_pred[i]]
        else:
            movement = '???'

        if (i > 0 and clases[total_pred[i]] != clases[total_pred[i - 1]]):
            same_prev = False

        if (i + 1 < total_pred.__len__() and clases[total_pred[i]] != clases[total_pred[i + 1]]):
            same_post = False
    else:
        movement = clases[total_pred[i - 1]]

    # if (same_post == False and same_prev == False):
    #     movement = clases[y_pred[i - 1]]

    cv2.putText(frame, 'doing: ' + movement, 
        bottomLeftCornerOfText, 
        font, 
        fontScale,
        fontColor,
        lineType)

    current_frame += 1

    if ret == True:
        # Display the resulting frame
        cv2.imshow('Frame', frame)
    
        # Press Q on keyboard to  exit
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break
        
        # if cv2.waitKey(25) & 0xFF == ord('p'):
        #     while(True):
        #         if cv2.waitKey(25) & 0xFF == ord('p'):
        #             break

    # Break the loop
    else: 
        break

# When everything done, release the video capture object
cap.release()

# Closes all the frames
cv2.destroyAllWindows()
    