import keras
import tensorflow as tf
import cv2
import itertools
import numpy as np
import os
import sys
from matplotlib import pyplot as plt
from keras.preprocessing import image
from keras import backend as K 
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, StratifiedKFold
from keras.layers import Dense, Conv2D, MaxPool2D, Dropout, Flatten
from keras.callbacks import ModelCheckpoint, EarlyStopping
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
import subprocess
import calendar
import time
import ffmpy

K.clear_session()

def load_data_2_channels(ts):
    data = []
    x = []
    train_flows = []
    files = os.listdir('./tmp/' + ts)
    for flow in range(1, int(files.__len__() / 2) + 1):

        flow_x = cv2.imread('./tmp/' + ts + '/flow_x' + str(flow) + '.jpg', cv2.IMREAD_GRAYSCALE)
        flow_y = cv2.imread('./tmp/' + ts + '/flow_y' + str(flow) + '.jpg', cv2.IMREAD_GRAYSCALE)

        # flow_x = cv2.flip(flow_x, 1)
        # flow_y = cv2.flip(flow_y, 1)

        # flow_x = fgbg.apply(flow_x)
        # flow_y = fgbg.apply(flow_y)

        
        gray_flow_x = cv2.resize(flow_x, (100, 100))
        # gray_flow_x = gray_flow_x[...,::-1]

        # cv2.imshow('frame', gray_flow_x)

        # if cv2.waitKey(25) & 0xFF == ord('q'):
        #     break


        gray_flow_y = cv2.resize(flow_y, (100, 100))
        # gray_flow_y = gray_flow_y[...,::-1]

        train_flows.append(cv2.merge((gray_flow_x, gray_flow_y)))

        if (train_flows.__len__() % 12 == 0):
            x.append(np.array(np.concatenate(train_flows, axis = -1))) 
            train_flows = []

    return np.array(x)


def load_data(raw_video_path):
    ts = str(calendar.timegm(time.gmtime()))
    os.mkdir('tmp/' + ts)
    avi_video = raw_video_path
    # avi_video = os.path.splitext(raw_video_path)[0] + '-' + ts + '.avi'
    # ff = ffmpy.FFmpeg(
    #     inputs={raw_video_path: None},
    #     outputs={avi_video: None}
    # )
    # ff.run()
    cap = cv2.VideoCapture(avi_video)
    total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    proc = subprocess.Popen(
        [
            './denseFlow_gpu', 
            '--vidFile=' + avi_video, 
            '--xFlowFile=' + 'tmp/' + ts + '/flow_x', 
            '--yFlowFile=' + 'tmp/' + ts + '/flow_y', 
            '--device_id=0',
            '--type=2',
            '--bound=16',
            '--clip=20000000',
            '--step=3'
        ], 
        stdout=subprocess.PIPE, 
        # bufsize=10
        stderr=subprocess.PIPE
    )

    prev = -1
    while True:
        out = proc.stdout.read(1)
        if out == b'':
            while True:
                err = proc.stderr.readline()
                if err == b'':
                    break
                else:
                    print(err)
            break
        if out != b'':
            files = os.listdir('./tmp/' + ts).__len__() / 2
            if prev != files: 
                prev = files
                sys.stdout.write("Analyzing: " +  str(round((files * 100) / (total_frames / 3))) + "%\n")
                sys.stdout.flush()

    # for line in iter(proc.stdout.readline, b''):
    #     print(line),
    
    # proc.stdout.close()
    # proc.wait()

    # stdout,stderr = process.communicate()

    # if (stderr == None):
    print('Processing...\n')
    return load_data_2_channels(ts)
    # return False


flows = load_data(sys.argv[1])
print(flows.shape)
if (isinstance(flows, bool)):
    sys.exit('Something failed')

# def calculate_scores(y_pred, y_probs)



filename = os.listdir('with_rest_v2')[0]
print('Using: ' + filename)
for weights in os.listdir('with_rest_v2/' + filename + '/training') :
    model = None

    if weights.endswith(".h5"):

        model = keras.models.load_model('with_rest_v2/' + filename + '/training/' + weights)

        y_pred = model.predict_classes(flows)
        y_pred2 = model.predict(flows)

        print(y_pred)
        print(y_pred2)
        print('\n')
    