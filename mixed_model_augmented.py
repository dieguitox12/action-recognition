import keras
import tensorflow as tf
import cv2
import itertools
import numpy as np
import os
from matplotlib import pyplot as plt
from keras.preprocessing import image
from keras import backend as K 
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, StratifiedKFold
from keras.layers import Dense, Conv2D, MaxPool2D, Dropout, Flatten
from keras.callbacks import ModelCheckpoint, EarlyStopping
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score

clases = ['rest', 'squat', 'pullup', 'pushup']
K.clear_session()
size = 100
batch = 30
def train(model, x_train, y_train, x_valid, y_valid, fold, metric):

    pat = 80 #this is the number of epochs with no improvment after which the training will stop
    early_stopping = EarlyStopping(monitor=metric, patience=pat, verbose=1)

    #define the model checkpoint callback -> this will keep on saving the model as a physical file
    model_checkpoint = ModelCheckpoint('best_mixed_augmented_fold_' + str(j) + '_' + metric + '.h5', verbose=1, save_best_only=True, monitor=metric)

    return model.fit(
        x_train, y_train,
        batch_size = batch,
        epochs = 200,
        validation_data=(x_valid, y_valid),
        callbacks=[early_stopping, model_checkpoint],
        verbose = 1
    )

def plot_confusion_matrix(cm, classes, title, acc,
                          normalize=False,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.gcf().subplots_adjust(bottom=0.4)
    plt.ylabel('True label')
    plt.xlabel('Predicted label\nAcuraccy: ' + acc)
    plt.savefig(title + '.png')
    plt.clf()
    
## multiclass or binary report
## If binary (sigmoid output), set binary parameter to True
def full_multiclass_report(model,
                           x,
                           y_true,
                           classes,
                           title,
                           batch_size=batch,
                           binary=False):

    # 1. Transform one-hot encoded y_true into their class number
    if not binary:
        y_true = np.argmax(y_true,axis=1)
    
    # 2. Predict classes and stores in y_pred
    y_pred = model.predict_classes(x, batch_size=batch_size)
    
    # 3. Print accuracy score
    print("Accuracy : "+ str(accuracy_score(y_true,y_pred)))
    
    print("")
    
    # 4. Print classification report
    print("Classification Report")
    print(classification_report(y_true,y_pred,digits=5))    
    
    # 5. Plot confusion matrix
    cnf_matrix = confusion_matrix(y_true,y_pred)
    print(cnf_matrix)
    plot_confusion_matrix(cnf_matrix,classes=classes, title=title, acc=str(accuracy_score(y_true,y_pred)))


def get_model(n):
    model = keras.Sequential()

    model.add(Conv2D(16, (7, 7), padding="same", input_shape=(size, size, 24), activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2), strides=(1, 1)))
    model.add(keras.layers.normalization.BatchNormalization(axis=-1))

    model.add(Conv2D(32, (5, 5), padding="same", activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2), strides=(1, 1)))

    model.add(Conv2D(64, (3, 3), padding="same", activation='relu', strides=(1, 1)))
    model.add(MaxPool2D(pool_size=(2, 2), strides=(1, 1)))

    model.add(Conv2D(64, (3, 3), padding="same", activation='relu', strides=(1, 1)))
    model.add(MaxPool2D(pool_size=(2, 2), strides=(1, 1)))

    model.add(Conv2D(64, (3, 3), padding="same", activation='relu', strides=(1, 1)))
    model.add(MaxPool2D(pool_size=(2, 2), strides=(1, 1)))

    model.add(Conv2D(64, (3, 3), padding="same", activation='relu', strides=(1, 1)))
    model.add(MaxPool2D(pool_size=(2, 2), strides=(1, 1)))

    model.add(Conv2D(64, (3, 3), padding="same", activation='relu', strides=(1, 1)))
    model.add(MaxPool2D(pool_size=(2, 2), strides=(1, 1)))

    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.1))

    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.1))

    model.add(Dense(n, activation='softmax'))

    model.compile(
        loss='categorical_crossentropy',
        optimizer='sgd',
        metrics=['accuracy']
    )

    return model

def plot_history(history, title):
    loss_list = [s for s in history.history.keys() if 'loss' in s and 'val' not in s]
    val_loss_list = [s for s in history.history.keys() if 'loss' in s and 'val' in s]
    acc_list = [s for s in history.history.keys() if 'acc' in s and 'val' not in s]
    val_acc_list = [s for s in history.history.keys() if 'acc' in s and 'val' in s]
    
    if len(loss_list) == 0:
        print('Loss is missing in history')
        return 
    
    ## As loss always exists
    epochs = range(1,len(history.history[loss_list[0]]) + 1)
    
    ## Loss
    plt.figure(1)
    for l in loss_list:
        plt.plot(epochs, history.history[l], 'b', label='Training loss (' + str(str(format(history.history[l][-1],'.5f'))+')'))
    for l in val_loss_list:
        plt.plot(epochs, history.history[l], 'g', label='Validation loss (' + str(str(format(history.history[l][-1],'.5f'))+')'))
    
    plt.title('Loss ' + title)
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.savefig('Loss_' + title + '.png')
    plt.clf()
    
    ## Accuracy
    plt.figure(2)
    for l in acc_list:
        plt.plot(epochs, history.history[l], 'b', label='Training accuracy (' + str(format(history.history[l][-1],'.5f'))+')')
    for l in val_acc_list:    
        plt.plot(epochs, history.history[l], 'g', label='Validation accuracy (' + str(format(history.history[l][-1],'.5f'))+')')

    plt.title('Accuracy ' + title)
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.savefig('Accuracy_' + title + '.png')
    plt.clf()

def load_data_2_channels(movement, obj):
    directories = open('./datasets/augmented/flow/' + movement + '/' + obj + '/files.txt', 'r')
    x_train = []
    y_train = []
    for video in directories:
        train_flows = []
        DIR = './datasets/augmented/flow/' + movement + '/' + obj + '/' + video.rstrip()
        total_files = 0
        total_files = len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))])
        if (total_files < 24):
            continue
        for flow in range(1, int(total_files / 2) + 1) :
            flow_x = cv2.imread('./datasets/augmented/flow/' + movement + '/' + obj + '/' + video.rstrip() + '/flow_x' + str(flow) + '.jpg', cv2.IMREAD_GRAYSCALE)
            flow_y = cv2.imread('./datasets/augmented/flow/' + movement + '/' + obj + '/' + video.rstrip() + '/flow_y' + str(flow) + '.jpg', cv2.IMREAD_GRAYSCALE)

            gray_flow_x = cv2.resize(flow_x, (size, size))
            gray_flow_x = gray_flow_x[...,::-1]

            gray_flow_y = cv2.resize(flow_y, (size, size))
            gray_flow_y = gray_flow_y[...,::-1]
            
            train_flows.append(cv2.merge((gray_flow_x, gray_flow_y)))
            if (train_flows.__len__() % 12 == 0):
                x_train.append(np.array(np.concatenate(train_flows, axis = -1)))
                y_train.append(clases.index(movement))
                train_flows = []

    x_train = np.array(x_train)
    # y_train = [[clases.index(movement)]] * len(data)
    # y_train = [clases.index(movement)] * len(data)
    return (x_train, y_train)


squat_x_train, squat_y_train = load_data_2_channels('squat', 'train')

pushup_x_train, pushup_y_train = load_data_2_channels('pushup', 'train')

pullup_x_train, pullup_y_train = load_data_2_channels('pullup', 'train')

rest_x_train, rest_y_train = load_data_2_channels('rest', 'train')

# Normalization

x_train = np.concatenate((squat_x_train, pushup_x_train, pullup_x_train, rest_x_train))
y_train = np.concatenate((squat_y_train, pushup_y_train, pullup_y_train, rest_y_train))

# x_train = x_train / 255


# Cross-validation

folds = list(StratifiedKFold(n_splits=5, shuffle=True, random_state=1).split(x_train, y_train))

y_train = keras.utils.to_categorical(y_train, len(clases))

# Model

#With cross-validation
for j, (train_idx, val_idx) in enumerate(folds):
    print('\nFold ',j)

    x_train_cv = None
    y_train_cv = None
    x_valid_cv = None
    y_valid_cv = None

    x_train_cv = x_train[train_idx]
    y_train_cv = y_train[train_idx]
    x_valid_cv = x_train[val_idx]
    y_valid_cv= y_train[val_idx]

    model = None
    del model

    model = get_model(len(clases))

    hist = train(model, x_train_cv, y_train_cv, x_valid_cv, y_valid_cv, j, 'val_acc')

    model.load_weights('best_mixed_augmented_fold_' + str(j) + '_val_acc.h5')

    full_multiclass_report(model, x_valid_cv, y_valid_cv, clases, title = 'Mixed_augmented_Fold_' + str(j) + '_Confusion_Matrix')

    plot_history(hist, 'Mixed_augmented_Fold_' + str(j))
