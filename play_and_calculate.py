#!/usr/bin/env python3

import keras
import tensorflow as tf
import cv2
import itertools
import numpy as np
import os
import sys
from matplotlib import pyplot as plt
from keras.preprocessing import image
from keras import backend as K 
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, StratifiedKFold
from keras.layers import Dense, Conv2D, MaxPool2D, Dropout, Flatten
from keras.callbacks import ModelCheckpoint, EarlyStopping
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
import subprocess
import calendar
import time
from PIL import Image

clases = ['rest', 'squat', 'pullup', 'pushup']
K.clear_session()
path = ''

def get_clusters(data):
    key = 0
    prev = None
    res = []

    for value in data:
        if prev == value:
            res[key] = str(res[key]) + ',' + str(value)
        else:
            if (key != 0):
                key = key + 1
            res.insert(key, str(value))
            prev = value

    res.reverse()
    return res

def load_y_data(ts):
    data = []
    x = []
    train_flows = []
    files = os.listdir('tmp/' + ts)
    for flow in range(1, int(files.__len__() / 2) + 1):

        flow_y_name = str(flow)
        
        # if flow < 10:
        #     flow_y_name = '00' + str(flow)
        # else:
        #     if flow < 100:
        #         flow_y_name = '0' + str(flow)


        flow_y = cv2.imread('tmp/' + ts + '/flow_y' + flow_y_name + '.jpg', cv2.IMREAD_GRAYSCALE)

        gray_flow_y = cv2.resize(flow_y, (100, 100))
        # gray_flow_y = gray_flow_y[...,::-1]
        

        train_flows.append(gray_flow_y)

        # if (train_flows.__len__() % 12 == 0):
        #     x.append(np.array(np.concatenate(train_flows, axis = -1))) 
        #     train_flows = []

    return train_flows

def load_data_2_channels(ts):
    data = []
    x = []
    train_flows = []
    files = os.listdir('tmp/' + ts)
    for flow in range(1, int(files.__len__() / 2) + 1):

        flow_x = cv2.imread('tmp/' + ts + '/flow_x' + str(flow) + '.jpg', cv2.IMREAD_GRAYSCALE)
        flow_y_name = str(flow)
        
        # if flow < 10:
        #     flow_y_name = '00' + str(flow)
        # else:
        #     if flow < 100:
        #         flow_y_name = '0' + str(flow)

        flow_y = cv2.imread('tmp/' + ts + '/flow_y' + flow_y_name + '.jpg', cv2.IMREAD_GRAYSCALE)

        gray_flow_x = cv2.resize(flow_x, (100, 100))
        # gray_flow_x = gray_flow_x[...,::-1]

        gray_flow_y = cv2.resize(flow_y, (100, 100))
        # gray_flow_y = gray_flow_y[...,::-1]

        train_flows.append(cv2.merge((gray_flow_x, gray_flow_y)))

        if (train_flows.__len__() % 12 == 0):
            x.append(np.array(np.concatenate(train_flows, axis = -1))) 
            train_flows = []

    return np.array(x)


def load_data(raw_video_path):
    ts = str(calendar.timegm(time.gmtime()))
    path = ts
    os.mkdir('tmp/' + ts)
    avi_video = raw_video_path

    cap = cv2.VideoCapture(avi_video)
    total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    proc = subprocess.Popen(
        [
            './denseFlow_gpu', 
            '--vidFile=' + avi_video, 
            '--xFlowFile=' + 'tmp/' + ts + '/flow_x', 
            '--yFlowFile=' + 'tmp/' + ts + '/flow_y', 
            '--device_id=0',
            '--type=2',
            '--bound=16',
            '--clip=20000000',
            '--step=3'
        ], 
        stdout=subprocess.PIPE, 
        stderr=subprocess.PIPE
    )

    prev = -1
    while True:
        out = proc.stdout.read(1)
        if out == b'':
            while True:
                err = proc.stderr.readline()
                if err == b'':
                    break
                else:
                    print(err)
            break
        if out != b'':
            files = os.listdir('./tmp/' + ts).__len__() / 2
            if prev != files: 
                prev = files
                sys.stdout.write("Analyzing: " +  str(round((files * 100) / (total_frames / 3))) + "%\n")
                sys.stdout.flush()

    return load_data_2_channels(ts)


if (sys.argv.__len__() == 3):
    path = sys.argv[2]
    flows =  load_data_2_channels(sys.argv[2])
else:
    flows = load_data(sys.argv[1])

if (isinstance(flows, bool)):
    sys.exit('Something failed')

current_frame = 0

movement = '???'

filename = os.listdir('with_rest_v2')[0]
total_pred = []
total_pred2 = []
for weights in os.listdir('with_rest_v2/' + filename + '/training') :
    model = None

    if weights.endswith(".h5"):

        current_frame = 0

        movement = '???'

        model = keras.models.load_model('with_rest_v2/' + filename + '/training/' + weights)

        # y_pred = model.predict_classes(flows)
        y_pred2 = model.predict(flows)

        total_pred2.append(y_pred2)

average = total_pred2[0]
for preds in range(1, total_pred2.__len__()):
    average += total_pred2[preds]

average = average / total_pred2.__len__()

# print(average)
total_pred = average.argmax(axis = 1)
cap = cv2.VideoCapture(sys.argv[1])
total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
fps = cap.get(cv2.CAP_PROP_FPS)      # OpenCV2 version 2 used "CV_CAP_PROP_FPS"


clusters = get_clusters(total_pred)

formatted_clusters = []
for cluster in clusters:
    new_cluster = cluster.split(',')
    formatted_clusters.append(new_cluster)


y_flows = load_y_data(path)

reps = []
pixels = 0.0
prev = 0.0
index = 0
for cluster in formatted_clusters:
    reps_per_cluster = []
    for pred in cluster:
        if (cluster.__len__() < 2 or pred == '0'):
            reps_per_cluster.append(0)
            prev = 0.0
        else:
            pixels = np.average(y_flows[index * 12] / 255)
            if (cluster.__len__() % 2 != 0):
                if (pixels < prev):
                    reps_per_cluster.append(1)
            else:
                if (pixels > prev):
                    reps_per_cluster.append(1)
            prev = pixels
        index = index + 1
    if (reps_per_cluster.__len__() > (cluster.__len__() / 2) and reps_per_cluster[0] == 1):
        reps_per_cluster.pop()
    reps.append(reps_per_cluster)

total_reps = []
# for rep in reps:
#     total_reps.append(rep)


print(*total_pred, sep = ",")
print(fps)
print(total_frames)
print(*reps, sep = "-")